namespace java ece454750s15a1 

struct Node {
    1: string host,
    2: i32 pport,
    3: i32 mport,
    4: i32 cores,
}

struct PerfCounters {
	//number of seconds since service startup
	1: i32 numSecondsUp,
	//total number of requests received by service handler
	2: i32 numRequestsReceived,
	// total number of requests completed by service handler
	3: i32 numRequestsCompleted
}

exception ServiceUnavailableException {
	1: string msg
}

service A1Password {
	string hashPassword(1:string password, 2:i16 logRounds) throws (1: ServiceUnavailableException e),
	bool checkPassword(1:string password, 2:string hash)
}

service A1Management {
	PerfCounters getPerfCounters(),
	list<string> getGroupMembers(),
    bool connect(1:string host, 2:i32 pport, 3:i32 mport, 4:i32 cores, 5:bool FE),
    list<Node> getBENodes(),
    bool addBENode(1:Node node),
    bool removeBENode(1:Node node),
}


