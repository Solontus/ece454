/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package ece454750s15a1;
import org.apache.thrift.TException;

// Generated code
import ece454750s15a1.*;

import org.mindrot.jbcrypt.BCrypt;

public class BEA1PasswordHandler implements A1Password.Iface {

  BEA1ManagementHandler managementHandler;

  public BEA1PasswordHandler(BEA1ManagementHandler handler) {
	managementHandler = handler;
  }

  public String hashPassword(String password, short logRounds) throws ServiceUnavailableException, TException{
	String hashed = BCrypt.hashpw(password, BCrypt.gensalt(logRounds));
	managementHandler.addReceived();
	managementHandler.addCompleted();
	return hashed;
  }

  public boolean checkPassword(String password, String hash) throws TException{
	managementHandler.addReceived();
	managementHandler.addCompleted();
	return BCrypt.checkpw(password, hash);
  }
}

