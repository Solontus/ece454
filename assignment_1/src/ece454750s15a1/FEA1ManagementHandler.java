
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package ece454750s15a1;

import org.apache.thrift.TException;

import org.apache.thrift.transport.TSSLTransportFactory;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TSSLTransportFactory.TSSLTransportParameters;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.protocol.TProtocol;
// Generated code
import ece454750s15a1.*;

import org.mindrot.jbcrypt.BCrypt;

import java.util.*;
import java.util.concurrent.*;

public class FEA1ManagementHandler implements A1Management.Iface {

  long start_time;
  int receivedRequest;
  int completedRequest;
  boolean isSeed;
  List<Node> BEList;
  List<Node> FEList;
  List<Node> SeedList;
  int index;
  int count;

  public FEA1ManagementHandler(boolean seed, List<Node> seedList) {
    isSeed = seed;
    BEList = new CopyOnWriteArrayList();
    FEList = new CopyOnWriteArrayList();
    SeedList = seedList;
	start_time = System.currentTimeMillis()/1000L;
    receivedRequest = 0;
    completedRequest = 0;
    index = 0;
    count = 0;
  }

  public PerfCounters getPerfCounters() {
  	PerfCounters perfCounters = new PerfCounters();
	perfCounters.numSecondsUp = (int) (System.currentTimeMillis()/1000L - start_time);
	perfCounters.numRequestsReceived = receivedRequest;
	perfCounters.numRequestsCompleted = completedRequest;
	return perfCounters;
  }

  public List<String> getGroupMembers() {
	List<String> members= new ArrayList<String>();
	members.add("ws3chau 20432797");
	members.add("zsherif 20418542");
	return members;
  }

  public boolean connect(String host, int pport, int mport, int cores, boolean FE) {
    if (isSeed) {
      Node connectedNode = new Node();
      connectedNode.host = host;
      connectedNode.pport = pport;
      connectedNode.mport = mport;
      connectedNode.cores = cores;
      if(!FE) {
        BEList.add(connectedNode);
        broadcastBENode(FEList, connectedNode, true);
      }
      else {
        FEList.add(connectedNode);
      }  
    }
    else {
      return false;
    }
    return true;
  }

  public Node getBEServer() {
    if(BEList.size() == 0) {
      return null;
    }
    else {
      if(index < BEList.size()) {
        if(count < BEList.get(index).getCores()) {
          count++;
          return BEList.get(index);
        }
        else {
          index++;
          count = 1;
        }
      }  
      if(index > BEList.size() - 1) {
        index = 0;
        count = 1;
      } 
      return BEList.get(index);
    }
  }

  private void broadcastBENode(List<Node> listOfNodes, Node node, boolean active) {
    for(Node FE: listOfNodes) {
      try {
        TTransport transport;
        transport = new TSocket(FE.host, FE.mport);
        transport.open();

        TProtocol protocol = new  TBinaryProtocol(new TFramedTransport(transport));
        A1Management.Client client = new A1Management.Client(protocol);
        if (active) {
          client.addBENode(node);
        }
        else {
          client.removeBENode(node);
        }
        transport.close();
      } catch (TException x) {
        System.out.println(x.getMessage());
        x.printStackTrace();
      }
    }
  }

  public List<Node> getBENodes() {
    if(isSeed) {
      return BEList;
    }
    else {
      return new ArrayList<Node>();
    }
  }

  public boolean addBENode(Node node) {
    if (!BEList.contains(node)) {
      BEList.add(node);
    }
    return true;
  }

  public boolean removeBENode(Node node) {
    boolean exists = BEList.remove(node);
    if (isSeed && exists) {
      broadcastBENode(FEList, node, false);
    }
    else if(!isSeed && exists) {
      broadcastBENode(SeedList, node, false);
    }
    return true;
  }

  public void setBEList(List<Node> seedList) {
    BEList.addAll(seedList);
  }    

  public void addReceived() {
	receivedRequest++;
  }

  public void addCompleted() {
	completedRequest++;
  }
}

