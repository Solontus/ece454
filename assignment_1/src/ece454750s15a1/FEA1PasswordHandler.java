
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package ece454750s15a1;
import org.apache.thrift.TException;
import org.apache.thrift.transport.TTransportException;

import org.apache.thrift.transport.TSSLTransportFactory;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TSSLTransportFactory.TSSLTransportParameters;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.protocol.TProtocol;
// Generated code
import ece454750s15a1.*;

import org.mindrot.jbcrypt.BCrypt;

public class FEA1PasswordHandler implements A1Password.Iface {

  FEA1ManagementHandler managementHandler;

  public FEA1PasswordHandler(FEA1ManagementHandler handler) {
	managementHandler = handler;
  }

  public String hashPassword(String password, short logRounds) throws ServiceUnavailableException, TException{
	managementHandler.addReceived();
    boolean successful = false;
    String hashed = "";
    while(!successful) {
      Node BENode = managementHandler.getBEServer();
      if(BENode == null) {
        throw new ServiceUnavailableException();
      }
      try {
        TTransport transport;
        transport = new TSocket(BENode.host, BENode.pport);
        transport.open();

        TProtocol protocol = new  TBinaryProtocol(new TFramedTransport(transport));
        A1Password.Client client = new A1Password.Client(protocol);

        hashed = client.hashPassword(password, logRounds);
        successful = true;
        transport.close();
      } catch (TTransportException x) {
        managementHandler.removeBENode(BENode);
        System.out.println("Removed node " + BENode.host + ":" + BENode.mport);
        System.out.println(x.getMessage());
      }
    } 
	managementHandler.addCompleted();
	return hashed;
  }

  public boolean checkPassword(String password, String hash) throws TException{
	managementHandler.addReceived();
    boolean checked = false;
    boolean successful = false;
    while(!successful) {
      Node BENode = managementHandler.getBEServer();
      if(BENode == null) {
          return false;
      }
      try {
        TTransport transport;
        transport = new TSocket(BENode.host, BENode.pport);
        transport.open();

        TProtocol protocol = new  TBinaryProtocol(new TFramedTransport(transport));
        A1Password.Client client = new A1Password.Client(protocol);

        checked = client.checkPassword(password, hash);
        successful = true;
        transport.close();
      } catch (Exception x) {
        managementHandler.removeBENode(BENode);
        System.out.println("Removed node " + BENode.host + ":" + BENode.mport);
      }
    } 
	managementHandler.addCompleted();
	return checked;
  }
}

