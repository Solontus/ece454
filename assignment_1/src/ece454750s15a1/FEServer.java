
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package ece454750s15a1;

import java.util.*;
import java.util.concurrent.*;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TServer.Args;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TSSLTransportFactory;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import org.apache.thrift.transport.TSSLTransportFactory.TSSLTransportParameters;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TThreadedSelectorServer;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TNonblockingServerTransport;
import org.apache.thrift.transport.TTransportException;
import org.apache.log4j.BasicConfigurator;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.TException;
import org.apache.thrift.transport.TTransport;

// Generated code
import ece454750s15a1.*;

public class FEServer {

  public static FEA1PasswordHandler passwordHandler;
  public static FEA1ManagementHandler managementHandler;

  public static A1Password.Processor passwordProcessor;
  public static A1Management.Processor managementProcessor;

  public static String host = "";
  public static int pport = 0;
  public static int mport = 0;
  public static int ncores = 0;
  public static CopyOnWriteArrayList<Node> seeds = new CopyOnWriteArrayList();

  public static void main(String [] args) {
	if (args.length < 10) {
	  System.out.println("Please provide command line arguments in the form of: -host <host> -pport <pport> -mport <mport> -ncores <ncores> -seeds <seeds>");
	  System.exit(0);
    }

  for (int i = 0; i < 10; i+=2) {
	if (args[i].equals("-host")) {
		host = args[i+1];
	}
	else if (args[i].equals("-pport")) {
		pport	= Integer.parseInt(args[i+1]);
    }
    else if (args[i].equals("-mport")) {
      mport = Integer.parseInt(args[i+1]);
    }
    else if (args[i].equals("-ncores")) {
      ncores = Integer.parseInt(args[i+1]);
    }
    else if (args[i].equals("-seeds")) {
      for(String seed : args[i+1].split(",")) {
        Node node = new Node();
        node.host = seed.split(":")[0];
        node.mport = Integer.parseInt(seed.split(":")[1]);
        seeds.add(node);
      }
    }
  }

	BasicConfigurator.configure();
    List<Node> initialBEList = new ArrayList();
	try {
      for(Node seed : seeds) {
        if(seed.host.equals(host) && seed.mport == mport) {
          continue;
        }
	    TTransport transport;
	    transport = new TSocket(seed.host, seed.mport);
	    transport.open();

	    TProtocol protocol = new TBinaryProtocol(new TFramedTransport(transport));
	    A1Management.Client client = new A1Management.Client(protocol);

        client.connect(host, pport, mport, ncores, true);
        initialBEList = client.getBENodes(); 
	    transport.close();
      }
	} catch (TException x) {
	  System.out.println(x.getMessage());
	  x.printStackTrace();
    }
    try {
	  managementHandler = new FEA1ManagementHandler(true, seeds);
      managementHandler.setBEList(initialBEList);
	  managementProcessor = new A1Management.Processor(managementHandler);
      
	  passwordHandler = new FEA1PasswordHandler(managementHandler);
      passwordProcessor = new A1Password.Processor(passwordHandler);

      Runnable password = new Runnable() {
        public void run() {
          Server.password(passwordProcessor, pport);
        }
      };      

	  Runnable management = new Runnable() {
	    public void run() {
		 Server.management(managementProcessor, mport);
	    }
	  };

      new Thread(password).start();
	  new Thread(management).start();
    } catch (Exception x) {
      x.printStackTrace();
    }
  }
}
