/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

// Generated code
import ece454750s15a1.*;

import java.util.concurrent.CountDownLatch;

import org.apache.thrift.TException;
import org.apache.thrift.transport.TSSLTransportFactory;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TSSLTransportFactory.TSSLTransportParameters;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.protocol.TProtocol;

import java.util.*;

public class JavaParallelClient {

  public static int[] mport = null;
  public static int[] pport = null;
  public static int logRounds = -1;
  public static int requests = -1;
  public static int workers = -1;
  public static String host = "localhost";
  public static long start_time = 0L;

  public static void main(String [] args) {

    if (args.length != 12) {
      System.out.println("Please enter command line arguments in the form of -host <host> -mport <mport> -pport <pport>  -logRounds <logRounds> -workers<workers>");
      System.exit(0);
    }

    start_time = System.currentTimeMillis();

    for (int i = 0; i < 12; i += 2) {
        if (args[i].equals("-mport")) {
            String[] ports = args[i+1].split(",");
            mport = new int[ports.length];
            for(int j = 0; j < ports.length; j++) {
              mport[j] = Integer.parseInt(ports[j]);
            }
        }
        else if (args[i].equals("-pport")) {
            String[] ports = args[i+1].split(",");
            pport = new int[ports.length];
            for(int j = 0; j < ports.length; j++) {
              pport[j] = Integer.parseInt(ports[j]);
            }
        }
        else if (args[i].equals("-host")) {
            host = args[i+1];
        }
        else if (args[i].equals("-logRounds")) {
            logRounds = Integer.parseInt(args[i+1]);
        }
        else if (args[i].equals("-requests")) {
            requests = Integer.parseInt(args[i+1]);
        }
        else if (args[i].equals("-workers")) {
            workers = Integer.parseInt(args[i+1]);
        }
        else {
            System.out.println("Please enter command line arguments in the form of -host <host> -mport <mport> -pport <pport> -requests <requests> -workers <workers>");
            System.exit(0);
        }
    }
    
    final CountDownLatch latch = new CountDownLatch(workers);

    for(int i = 0; i < workers; i++) {
      final int i_copy = i;
      new Thread() {
      public void run() {
        for(int j = 0; j < requests; j++) {
        try {
          TTransport transport;
          transport = new TSocket(host, pport[i_copy%pport.length]);
          transport.open();

          TProtocol protocol = new  TBinaryProtocol(new TFramedTransport(transport));
          A1Password.Client client = new A1Password.Client(protocol);

          perform(client);

          transport.close();
        } catch (TException x) {
	      System.out.println(x.getMessage());
          x.printStackTrace();
        }}
      latch.countDown();
      }}.start();
    }
    try {
        latch.await();
    }
    catch(Exception e) {
        e.printStackTrace();
    }

    System.out.println("Used " + (System.currentTimeMillis() - start_time) + "ms to process " + (requests * workers) + " requests");
	try {
	  TTransport transport;
	  transport = new TSocket(host, mport[0]);
	  transport.open();

	  TProtocol protocol = new TBinaryProtocol(new TFramedTransport(transport));
	  A1Management.Client client = new A1Management.Client(protocol);
	  
	  perform(client);
	  transport.close();
	} catch (TException x) {
	  System.out.println(x.getMessage());
	  x.printStackTrace();
    } 
  }

  private static void perform(A1Password.Client client) throws TException
  {
	String hashed = client.hashPassword("password", (short)logRounds);
	System.out.println("Hashed 'password' : " + hashed);

	boolean checked = client.checkPassword("password", hashed);
	System.out.println("Checked 'password' and '" + hashed + "' : " + checked);
  }

  private static void perform(A1Management.Client client) throws TException
  {
	PerfCounters counters = client.getPerfCounters();
	System.out.println("Server up for " + counters.numSecondsUp + " seconds");
    System.out.println("Server received " + counters.numRequestsReceived + " requests");
	System.out.println("Server completed " + counters.numRequestsCompleted + " requests");	
	List<String> members = client.getGroupMembers();
	System.out.println("Member 1: " + members.get(0));
  }
}
