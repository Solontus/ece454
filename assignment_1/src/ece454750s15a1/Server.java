/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package ece454750s15a1;

import java.util.*;

import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TServer.Args;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TSSLTransportFactory;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import org.apache.thrift.transport.TSSLTransportFactory.TSSLTransportParameters;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TThreadedSelectorServer;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TNonblockingServerTransport;
import org.apache.thrift.transport.TTransportException;
import org.apache.log4j.BasicConfigurator;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.TException;
import org.apache.thrift.transport.TTransport;

// Generated code
import ece454750s15a1.*;

public class Server { 
public static void password(A1Password.Processor processor, int pport) {
    try {
      TNonblockingServerTransport trans = new TNonblockingServerSocket(pport);
      TThreadedSelectorServer.Args args = new TThreadedSelectorServer.Args(trans);
      args.transportFactory(new TFramedTransport.Factory());
      args.protocolFactory(new TBinaryProtocol.Factory());
      args.processor(processor);
      args.selectorThreads(2);
      args.workerThreads(2);
      TServer server = new TThreadedSelectorServer(args);

      System.out.println("Starting the password server...");
      server.serve();
    } catch (Exception e) {
      System.out.println("Exception!");
      e.printStackTrace();
    }
  }

  public static void management(A1Management.Processor processor, int mport) {
    try {
      TNonblockingServerTransport trans = new TNonblockingServerSocket(mport);
      TThreadedSelectorServer.Args args = new TThreadedSelectorServer.Args(trans);
      args.transportFactory(new TFramedTransport.Factory());
      args.protocolFactory(new TBinaryProtocol.Factory());
      args.processor(processor);
      args.selectorThreads(2);
      args.workerThreads(2);
      TServer server = new TThreadedSelectorServer(args);

      System.out.println("Starting the management server...");
      server.serve();
    } catch (Exception e) {
      System.out.println("Exception!");
      e.printStackTrace();
    }
  }

}